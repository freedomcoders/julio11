<?php

namespace App\Http\Controllers;

use App\Models\Provincia;

class HomeController extends Controller
{
    /**
     * Show Main Page
     */
    public function index()
    {
        return view('welcome')->with('images',Provincia::get());
    }
}
