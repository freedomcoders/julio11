<?php

namespace App\Http\Controllers;

use App\Models\Municipio;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class MunicipioController extends Controller
{
    /**
     * Show Municipio Data
     * @param Municipio $municipio
     * @return Application|Factory|View
     */
    public function show(Municipio $municipio)
    {
        return view('provincias.show')->with('images', $municipio->media);
    }
}
