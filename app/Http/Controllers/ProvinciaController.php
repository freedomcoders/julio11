<?php

namespace App\Http\Controllers;

use App\Models\Provincia;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ProvinciaController extends Controller
{
    /**
     * Show Province Data
     * @param Provincia $provincia
     * @return Application|Factory|View
     */
    public function show(Provincia $provincia)
    {
        return view('provincias.show')->with(['images' => $provincia->municipios]);
    }
}
