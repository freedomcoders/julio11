<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Abusador extends Model
{
    use Uuids;
    use HasFactory;

    protected $table = 'abusadores';

    protected $fillable = ['name', 'phone_number', 'address', 'facebook_link', 'instagram_link'];
}
