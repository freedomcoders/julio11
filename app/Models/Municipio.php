<?php

namespace App\Models;

use App\Services\Slug\HasSlug;
use App\Services\Slug\SlugOptions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Municipio extends Model
{
    use HasFactory, HasSlug;

    protected $fillable = ['name', 'provincia_id','slug'];

    /**
     * @return BelongsTo
     */
    public function provincia()
    {
        return $this->belongsTo(Provincia::class);
    }

    public function media()
    {
        return $this->hasMany(Media::class);
    }

    /**
     * @return SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return (new SlugOptions())
            ->generateSlugsFrom('name');
    }

    /**
     * Get the route key name for Laravel.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

}
