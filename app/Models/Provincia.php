<?php

namespace App\Models;

use App\Services\Slug\HasSlug;
use App\Services\Slug\SlugOptions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Provincia extends Model
{
    use HasFactory, HasSlug;

    protected $fillable = ['name', 'slug'];

    /**
     * @return HasMany
     */
    public function municipios()
    {
        return $this->hasMany(Municipio::class);
    }

    /**
     * @return SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return (new SlugOptions())
            ->generateSlugsFrom('name');
    }

    /**
     * Get the route key name for Laravel.
     *
     * @return string
     */
    public function getRouteKeyName(): string
    {
        return 'slug';
    }
}
