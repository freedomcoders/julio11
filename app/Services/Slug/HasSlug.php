<?php

namespace App\Services\Slug;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait HasSlug
{
    /**
     * @return SlugOptions
     */
    abstract public function getSlugOptions(): SlugOptions;

    /**
     * Boot Has Slug Function
     */
    protected static function bootHasSlug(): void
    {
        static::creating(
            function (Model $model) {
                $slug_options = $model->getSlugOptions();
                $model->{$slug_options->slug_field} = $model->generateSlug($model, $slug_options);
            }
        );
    }

    /**
     * @param Model $model
     * @param SlugOptions $slug_options
     * @return string
     */
    protected function generateSlug(Model $model, SlugOptions $slug_options): string
    {
        $generated_slug = $this->generateSlugs($slug_options);
        $taken = $model->where($slug_options->slug_field, 'LIKE', "{$generated_slug}%")
            ->limit(count($slug_options->slugs_formats))
            ->count();
        return ($taken === 0)
            ? $generated_slug
            : $this->generateSlugs($slug_options, $taken);
    }

    /**
     * @param SlugOptions $slug_options
     * @param int $position
     * @return string
     */
    protected function generateSlugs(SlugOptions $slug_options, $position = 0): string
    {
        $slug_from = $this->{$slug_options->slug_from};
        if ($position == 0) {
            return Str::slug($slug_from);
        }
        if ($position < count($slug_options->slugs_formats)) {
            $format = Str::after($slug_options->slugs_formats[$position], '-');
            return Str::slug($slug_from . " " . $slug_options->getDate()->format($format));
        }
        return Str::slug(uniqid($slug_from . ' '));
    }
}
