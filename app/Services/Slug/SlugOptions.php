<?php

namespace App\Services\Slug;

use Carbon\Carbon;

class SlugOptions
{
    public string $slug_from;
    public string $slug_field = 'slug';
    public array $slugs_formats = ['slug', 'slug-y', 'slug-Y', 'slug-m-y', 'slug-z'];

    /**
     * SlugOptions constructor.
     * @param Carbon|null $date
     */
    public function __construct(public ?Carbon $date = null)
    {
    }

    /**
     * @param string $slug_from
     * @return $this
     */
    public function generateSlugsFrom(string $slug_from): self
    {
        $this->slug_from = $slug_from;
        return $this;
    }

    /**
     * @param string $field_name
     * @return $this
     */
    public function saveSlugsTo(string $field_name): self
    {
        $this->slug_field = $field_name;
        return $this;
    }

    /**
     * @param Carbon $date_field
     * @return $this
     */
    public function fromDate(Carbon $date_field): self
    {
        $this->date = $date_field;
        return $this;
    }

    /**
     * @param array $formats
     * @return SlugOptions
     */
    public function formats(array $formats): self
    {
        $this->slugs_formats = $formats;
        return $this;
    }

    /**
     * @return Carbon
     */
    public function getDate(): Carbon
    {
        return ($this->date instanceof Carbon) ? $this->date : Carbon::now();
    }

}
