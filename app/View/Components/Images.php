<?php

namespace App\View\Components;

use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;

class Images extends Component
{

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public Model $image)
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.images')->withImage($this->image);
    }

}
