<?php

namespace Database\Factories;

use App\Models\Abusador;
use Illuminate\Database\Eloquent\Factories\Factory;

class AbusadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Abusador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id'                 => $this->faker->uuid,
            'name'               => $this->faker->name,
            'email'              => $this->faker->unique()->safeEmail(),
            'profile_photo'      => $this->faker->imageUrl,
            'phone_number'       => $this->faker->phoneNumber,
            'address'            => $this->faker->streetAddress,
            'facebook'           => $this->faker->url,
            'twitter'            => $this->faker->url,
            'instagram'          => $this->faker->url,
            'verification_votes' => $this->faker->numberBetween($min = 0, $max = 10),
            'notes'              => $this->faker->text
        ];
    }
}
