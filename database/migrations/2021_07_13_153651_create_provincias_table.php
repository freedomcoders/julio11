<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvinciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'provincias',
            function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique();
                $table->string('slug')->unique();
                $table->timestamps();
            }
        );
        $date = \Carbon\Carbon::now();
        \App\Models\Provincia::insert(
            [
                ['name' => 'Pinar del Río', 'slug' => 'pinar-del-rio', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Artemisa', 'slug' => 'artemisa', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'La Habana', 'slug' => 'la-habana', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Mayabeque', 'slug' => 'mayabeque', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Matanzas', 'slug' => 'matanzas', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Cienfuegos', 'slug' => 'cienfuegos', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Villa Clara', 'slug' => 'villa-clara', 'created_at' => $date, 'updated_at' => $date],
                [
                    'name' => 'Sancti Spíritus',
                    'slug' => 'sancti-spiritus',
                    'created_at' => $date,
                    'updated_at' => $date
                ],
                ['name' => 'Ciego de Ávila', 'slug' => 'ciego-de-avila', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Camagüey', 'slug' => 'camagüey', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Las Tunas', 'slug' => 'las-tunas', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Holguín', 'slug' => 'holguin', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Granma', 'slug' => 'granma', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Santiago de Cuba','slug' => 'santiago-de-cuba', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Guantánamo','slug' => 'guantanamo', 'created_at' => $date, 'updated_at' => $date],
                ['name' => 'Isla de la Juventud','slug' => 'la-isla', 'created_at' => $date, 'updated_at' => $date],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provincias');
    }
}
