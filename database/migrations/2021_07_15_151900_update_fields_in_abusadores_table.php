<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateFieldsInAbusadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('abusadores', function (Blueprint $table) {
            //
            $table->renameColumn('facebook_link', 'facebook');
            $table->renameColumn('instagram_link', 'instagram');
            $table->string('twitter')->after('instagram_link');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('abusadores', function (Blueprint $table) {
            //
            $table->renameColumn('facebook', 'facebook_link');
            $table->renameColumn('instagram', 'instagram_link');
            $table->dropColumn('twitter');
        });
    }
}
