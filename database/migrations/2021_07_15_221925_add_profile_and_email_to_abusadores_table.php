<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfileAndEmailToAbusadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('abusadores', function (Blueprint $table) {
            //
            $table->string('email')->after('name');
            $table->string('profile_photo')->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('abusadores', function (Blueprint $table) {
            //
            $table->dropColumn('email');
        });
    }
}
