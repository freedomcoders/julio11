<?php

namespace Database\Seeders;

use App\Models\Abusador;
use Illuminate\Database\Seeder;

class AbusadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Abusador::factory()
            ->count(10)
            ->create();
    }
}
