<div class="category-1 mix custom-column-5" style="display: inline-block;">
    <div class="be-post">
        {{--        TODO: Make this more dynamic--}}
        {{--        {{ Route::current()->getName() }}--}}

        @if (Route::current()->getName() == 'home')
            @php
                $url = route('provincias.show',['provincia'=> $image->slug])
            @endphp
        @elseif (Route::current()->getName() == 'provincias.show')
            @php
                $url = route('municipios.show', ['municipio'=> $image->slug])
            @endphp
        @elseif (Route::current()->getName() == 'municipios.show')
            {{--            <a href="{{route('abusadores.show',--}}
            {{--                ['provincia' => $image->provincia, 'municipio' => $image->provincia, 'abusador'=> $image->slug])}}" class="be-post-title">{{$image->name}}</a>--}}
        @elseif (Route::current()->getName() == 'abusadores.index')
            @php
                $url = route('abusadores.show', ['abusadore'=> $image->id]);
                $photo = $image->profile_photo
            @endphp
        @endif

        <a href="{{ $url ?? '' }}" class="be-img-block">
            <img src="{{ $photo ?? '/img/p1.jpg' }}" alt="omg">
        </a>
        <a href="{{ $url ?? '' }}" class="be-post-title">{{ $image->name ?? ''}}</a>

        <span>
            Abusadores Identificados:
        </span>
        <span>
                <a href="blog-detail-2.html" class="be-post-tag" >Juan Roche</a>,
                <a href="blog-detail-2.html" class="be-post-tag">Miguel Diez</a>
        </span>
{{--        <div class="author-post">--}}
{{--            <img src="/img/a1.png" alt="" class="ava-author">--}}
{{--            <span>by <a href="page1.html">Hoang Nguyen</a></span>--}}
{{--        </div>--}}
        <div class="info-block">
            <span><i class="fa fa-thumbs-o-up"></i> 360</span>
            <span><i class="fa fa-eye"></i> 789</span>
            <span><i class="fa fa-comment-o"></i> 20</span>
        </div>
    </div>
</div>
