<div class="footer_slider">
    <div class="swiper-container" data-autoplay="0" data-loop="1" data-speed="500" data-center="0" data-slides-per-view="responsive" data-xs-slides="4" data-sm-slides="8" data-md-slides="14" data-lg-slides="19" data-add-slides="19">
        <div class="swiper-wrapper">
            <div class="swiper-slide active" data-val="0">

                <a href="gallery.html">				<img class="img-responsive img-full" src="img/f1.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="1">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f2.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="2">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f3.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="3">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f4.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="4">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f5.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="5">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f6.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="6">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f7.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="7">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f8.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="8">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f9.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="9">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f10.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="10">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f11.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="11">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f12.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="12">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f13.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="13">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f14.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="14">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f15.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="15">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f16.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="16">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f17.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="17">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f18.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="18">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f19.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="19">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f1.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="20">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f2.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="21">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f3.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="22">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f4.jpg" alt="">
                </a></div>
            <div class="swiper-slide" data-val="23">
                <a href="gallery.html">

                    <img class="img-responsive img-full" src="img/f5.jpg" alt="">
                </a></div>
        </div>
        <div class="pagination hidden"></div>
    </div>
</div>
<div class="footer-main">
    <div class="container-fluid custom-container">
        <div class="row">
            <div class="col-md-3 col-xl-4">
                <div class="footer-block">
                    <h1 class="footer-title">About Us</h1>
                    <p>En Julio 11, 2021 el pueblo de Cuba sale a las calle a demostrar pacificamente su frustracion de
                        62 años contra el govierno. Nuestra pagina Cataloga los abusos de derechos humanos del regimen
                        comunista en Cuba y identificamos a sus miembros que maltratan a nuestro pueblo.</p>
                    <ul class="soc_buttons">
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/julio11cuba"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/julio11cuba/"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-xl-2">
                <div class="footer-block">
                    <h1 class="footer-title">Links</h1>
                    <div class="row footer-list-footer">
                        <div class="col-md-6">
                            <ul class="link-list">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li><a href="{{ route('abusadores.index') }}">Abusadores</a></li>
                                <li><a href="{{ route('provincias.index') }}">Provincias</a></li>
                                <li><a href="{{ route('municipios.index') }}">Municipios</a></li>
                                <li><a href="{{ route('contactUs.index') }}">Contact Us</a></li>
                            </ul></div>
{{--                        <div class="col-md-6">--}}
{{--                            <ul class="link-list">--}}
{{--                                <li><a href="activity.html">New Works</a></li>--}}
{{--                                <li><a href="author.html">Popular Authors</a></li>--}}
{{--                                <li><a href="author.html">New Authors</a></li>--}}
{{--                                <li><a href="people.html">Career</a></li>--}}
{{--                                <li><a href="faq">FAQ</a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-3 galerry">
                <div class="footer-block">
                    <h1 class="footer-title">Recent Works</h1>
                    <a href="blog-detail-2.html"><img src="img/g1.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g2.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g3.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g4.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g5.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g6.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g7.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g8.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g9.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g10.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g11.jpg" alt=""></a>
                    <a href="blog-detail-2.html"><img src="img/g12.jpg" alt=""></a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-block">
                    <h1 class="footer-title">Subscribete a nuestras noticias</h1>
                    <form action="./" class="subscribe-form">
                        <input type="text" placeholder="Yout Name" required>
                        <div class="submit-block">
                            <i class="fa fa-envelope-o"></i>
                            <input type="submit" value="">
                        </div>
                    </form>
{{--                    <div class="soc-activity">--}}
{{--                        <div class="soc_ico_triangle">--}}
{{--                            <i class="fa fa-twitter"></i>--}}
{{--                        </div>--}}
{{--                        <div class="message-soc">--}}
{{--                            <div class="date">16h ago</div>--}}
{{--                            <a href="blog-detail-2.html" class="account">@faq</a> vestibulum accumsan est <a href="blog-detail-2.html" class="heshtag">blog-detail-2.htmlmalesuada</a> sem auctor, eu aliquet nisi ornare leo sit amet varius egestas.--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footer-bottom">
    <div class="container-fluid custom-container">
        <div class="col-md-12 footer-end clearfix">
            <div class="left">
                <span class="copy">© 2021. All rights reserved. <span class="white"><a href="blog-detail-2.html"> Julio 11</a></span></span>
                <span class="created">Created by <span class="white"><a href="blog-detail-2.html"> FREEDOM Coders</a></span></span>
            </div>
            <div class="right">
                <a class="btn color-7 size-2 hover-9">About Us</a>
                <a class="btn color-7 size-2 hover-9">Help</a>
                <a class="btn color-7 size-2 hover-9">Privacy Policy</a>
            </div>
        </div>
    </div>
</div>
