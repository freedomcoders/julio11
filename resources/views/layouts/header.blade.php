<div class="container-fluid custom-container">
    <div class="row no_row row-header">
        <div class="brand-be">
            <a href="/">
                <img class="logo-c active be_logo img-responsive" style="max-width: 150px"  src="/img/logo-julio11.png" alt="logo">
            </a>
        </div>
        <div class="header-menu-block">
            <button class="cmn-toggle-switch cmn-toggle-switch__htx"><span></span></button>
            <ul class="header-menu" id="one">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('abusadores.index') }}">Abusadores</a></li>
                <li><a href="{{ route('provincias.index') }}">Provincias</a></li>
                <li><a href="{{ route('municipios.index') }}">Municipios</a></li>
                <li><a href="{{ route('contactUs.index') }}">Contact Us</a></li>
            </ul>
        </div>
        <div class="login-header-block hidden">
            <div class="login_block">
                <a class="btn-login btn color-1 size-2 hover-2" href="" ><i class="fa fa-user"></i>
                    Log in</a>
            </div>
        </div>
    </div>
</div>
