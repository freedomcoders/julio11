<div class="col-md-2 left-feild">
    <div class="be-vidget">
        <h3 class="letf-menu-article">
            Provincias
        </h3>
        <div class="creative_filds_block">

            <div class="ul">
                @foreach($provincias as $provincia)
                    <a href="{{route('provincias.show',['provincia'=>$provincia->slug])}}" class="filter">{{$provincia->name}} </a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="be-vidget">
        <h3 class="letf-menu-article">
            Popular Tags
        </h3>
        <div class="tags_block clearfix">
            <ul>
                <li><a data-filter=".category-6" class="filter">photoshop</a></li>
                <li><a data-filter=".category-1" class="filter">graphic</a></li>
                <li><a data-filter=".category-2" class="filter">art</a></li>
                <li><a data-filter=".category-3" class="filter">website</a></li>
                <li><a data-filter=".category-4" class="filter">logo</a></li>
                <li><a data-filter=".category-5" class="filter">identity</a></li>
                <li><a data-filter=".category-6" class="filter">logo design</a></li>
                <li><a data-filter=".category-1" class="filter">interactive</a></li>
                <li><a data-filter=".category-2" class="filter">blue</a></li>
                <li><a data-filter=".category-3" class="filter">branding</a></li>
            </ul>
        </div>
    </div>
</div>
