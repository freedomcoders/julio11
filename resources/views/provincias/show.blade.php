@extends('layouts.master')
@section('content')
    <div id="container-mix" class="row _post-container_">
        @foreach($images as $image)
            <x-images :image="$image"/>
        @endforeach
    </div>
@endsection
