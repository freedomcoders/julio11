@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="login_block">
                <a class="btn-login btn color-1 size-2 hover-1" href=""><i class="fa fa-user"></i>
                    Adicionar Images </a>
            </div>
        </div>
    </div>
    <div id="container-mix" class="row _post-container_" style="margin-top: 10px">
        @foreach($images as $image)
            <x-images :image="$image"/>
        @endforeach
    </div>
@endsection
