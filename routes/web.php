<?php

use App\Http\Controllers\AbusadoresController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProvinciaController;
use App\Http\Controllers\MunicipioController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::resources([
    'abusadores' => AbusadoresController::class,
    'provincias' => ProvinciaController::class,
    'municipios' => MunicipioController::class,
    'contactUs' => ContactUsController::class,
]);
//Route::get('/provincia/{provincia}', [ProvinciaController::class, 'show'])->name('provincia.show');

